# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

import libtcodpy as tcod

from pythaum import config

font_layouts = {
    'columns': tcod.FONT_LAYOUT_ASCII_INCOL,
    'rows': tcod.FONT_LAYOUT_ASCII_INROW,
    'simple': tcod.FONT_LAYOUT_TCOD,
}

class Window(object):
    def __init__(self):
        screen_prefs = config.prefs['screen']

        font_prefs = screen_prefs['font']
        fontfile = config.find_datafile(font_prefs['file'], config.fontpaths)
        layout = font_layouts[font_prefs['layout']]
        if font_prefs.get('greyscale'):
            layout |= tcod.FONT_TYPE_GREYSCALE

        tcod.console_set_custom_font(fontfile, layout)
        
        width, height = screen_prefs['width'], screen_prefs['height']
        tcod.console_init_root(width, height,
                config.prefs['game']['title'], screen_prefs['fullscreen'])

        self.con = tcod.console_new(width, height)

        tcod.sys_set_fps(screen_prefs['fps_cap'])


