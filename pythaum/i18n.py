# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt


import os, sys
import gettext
import locale

from pythaum import config

lang = config.prefs['locale']['lang'] or locale.getdefaultlocale()[0]

gettext.install(True, localedir=config.l10npaths, unicode=1)
#filename = "res/messages_%s.mo" % locale.getlocale()[0][0:2]
