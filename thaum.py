# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt


import os, sys
import argparse

#from pythaum import Realm, RogUI
import pythaum
from pythaum import config
from pythaum import rogue

#import gettext
#t = gettext.translation('default', config.l10npaths)
#_ = t.ugettext

def main():
    progdesc = '''Thaumarcology is a strategy roguelike game in which the player
    oversees the construction and defense of a magical city.'''

    parser = argparse.ArgumentParser(description=progdesc)
    # TODO

    rogue.start()

if __name__ == '__main__': main()
