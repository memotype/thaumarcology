# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt

print "Loading Thaumarcology..."

# Path to the install directory. Declared here so other modules don't have to
# be updated with fewer/more os.path.dirname calls if they get moved around in
# the source tree. TODO: Use setup.py to set this since we're really going to
# be looking for /usr/local/share/thaumarcology or something...
from os import path
instpath = path.dirname(path.realpath(__file__))

import sys
import argparse

import config
import util
print "  Loading i18n subsystem...",
sys.stdout.flush()
import i18n
print "done."
sys.stdout.flush()

print "  Loading realm subsystem...",
sys.stdout.flush()
from realm import Realm
print "done."
sys.stdout.flush()

print "  Loading roguelike UI...",
sys.stdout.flush()
import rogue
print "done."
sys.stdout.flush()

#print "  Initializing plugins...",
#sys.stdout.flush()
#import plugin
#print "running."
#sys.stdout.flush()

print
sys.stdout.flush()

# Clean up imported modules that aren't needed in this namespace.
del sys
del path
