# Copyright (c) 2013, Isaac Freeman <memotype@gmail.com>
# All rights reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
# The full license is also available in the file LICENSE.apache-2.0.txt


# This is for global game / interface config stuff. Per-realm config will be
# stored with the realm saves.

import os, sys
from os.path import join

import yaml
from xdg import BaseDirectory as basedir

from pythaum import instpath
from pythaum.util import WrapDict

resname = 'thaumarcology'

# Locations of the data files and saves.

datapaths = list(basedir.load_data_paths(resname))
#datapaths.append(join(os.getcwd(), 'data'))
print "datapaths", datapaths
l10npaths = [join(dp, 'l10n') for dp in datapaths]
plugpaths = [join(dp, 'plugins') for dp in datapaths] 
#plugpaths.append(join(os.getcwd(), 'pythaum', 'plugins'))
fontpaths = [join(dp, 'fonts') for dp in datapaths]

savepath = join(basedir.save_data_path(resname), 'saves')


# Parse and load defaults

confpaths = list(basedir.load_config_paths(resname))
confpaths.append(join(os.getcwd(), 'data'))

defaults = {}
#import ipdb; ipdb.set_trace()
for confpath in reversed(datapaths):
    _conffilename = join(confpath, 'defaults.yaml')
    if os.path.exists(_conffilename):
        with file(_conffilename, 'r') as _conffile:
            defaults.update(yaml.load(_conffile))


# Parse and load the user's local preferences (global/non-per-realm options)

prefs = WrapDict(defaults)
preffile = join(basedir.save_config_path(resname), 'prefs.yaml')
if not os.path.exists(preffile):
    file(preffile, 'a').close()
with file(preffile, 'r') as _preffile:
    prefs.update(yaml.load(_preffile) or {})

# Functions to save prefs and file data files
def save(prefs=prefs):
    with file(preffile, 'w') as _preffile:
        yaml.dump(prefs.copy(), _preffile, default_flow_style=False)

def find_datafile(filename, paths=datapaths):
    for path in paths:
        datafilename = join(path, filename)
        if os.path.exists(datafilename):
            return datafilename
    raise IOError('File not found in data paths: {0}'.format(filename))
